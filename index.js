let http = require('http');

let server = http.createServer((req, res) => {
  res.writeHead(200, {'Content-Type': 'text/html'});
  res.end('<html>Hi</html>');
});

server.listen(process.argv[2]);